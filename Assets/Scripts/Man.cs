﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Man : MonoBehaviour
{
    [SerializeField] private float eyeHeight;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float jumpForce;
    [SerializeField] private Rigidbody rigidbody;

    private Vector3 cameraPosition;
    private Vector3 cameraAngles;

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        cameraAngles = transform.eulerAngles;
    }

    void Update()
    {
        Look();
        Move();
        Jump();
    }

    void Look()
    {
        cameraPosition = transform.position + new Vector3(0f, eyeHeight, 0f);

        cameraAngles += new Vector3(Input.GetAxis("looky"), Input.GetAxis("lookx"), 0f);
        cameraAngles.x = Mathf.Clamp(cameraAngles.x, -90f, 90f);

        Camera.main.transform.position = cameraPosition;
        Camera.main.transform.eulerAngles = cameraAngles;
    }

    void Move()
    {
        Vector3 forward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1f, 0f, 1f)).normalized;
        Vector3 right = Camera.main.transform.right;
        Vector3 velocity = (forward * Input.GetAxis("movey") + right * Input.GetAxis("movex")) * moveSpeed;
        rigidbody.velocity = new Vector3(velocity.x, rigidbody.velocity.y, velocity.z);
    }

    void Jump()
    {
        if (!Physics.CheckSphere(transform.position - new Vector3(0f, 0.7f, 0f), 0.4f))
            return;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Vector3 velocity = rigidbody.velocity;
            velocity.y = jumpForce;
            rigidbody.velocity = velocity;
        }
    }
}
