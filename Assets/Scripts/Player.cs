﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private Man man;
    float x = 0f;

	void Update()
    {
        if (man.transform.position.x > x)
        {
            Vector3 position = transform.position;
            position.x = man.transform.position.x;
            x = position.x;
            transform.position = position;
        }
    }
}
